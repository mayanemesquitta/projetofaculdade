#Função para realizar o calculo da escala AVCH
def calcular_avch(ecg, idade, l_hematoma, v_hematoma, hemoventriculo):
    score_avch = int(ecg) + int(idade) + int(l_hematoma) + int(v_hematoma) + int(hemoventriculo)
    mensagem = calculo_legendaavch(score_avch)

    return score_avch, mensagem
#Função para definir a legenda da escala AVCH
def calculo_legendaavch(valor):
    mensagem: ''
    if valor == 6 or valor == 5:
        mensagem = '100%'
    if valor == 4:
        mensagem = '97%'
    if valor == 3:
        mensagem = '72%'
    if valor==2:
        mensagem = '26%'
    if valor== 1:
        mensagem = '13%'
    if valor ==0:
        mensagem = '0%'

    return mensagem