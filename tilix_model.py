#Função para realizar o calculo da escala TILICS
def calcular_tilix(morfologiaFratura, complexoLigamentar, estadoNeurologico):
    resultado = int(morfologiaFratura) + int(complexoLigamentar) + int(estadoNeurologico)
    mensagem = legendatilix(resultado)
    return resultado, mensagem
#Função para definir alegenda escala TILICS
def legendatilix(valor):
    mensagem = ''
    if (valor <= 3):
        mensagem = 'Tratamento Conservador'
    elif (valor == 4):
        mensagem = 'Cirurgico ou Conservador'
    elif (valor >= 5):
        mensagem = 'Tratamento Cirurgico'
    return mensagem
