from flask import Flask, render_template, request, redirect, url_for
from avch_model import calcular_avch
from ecg_model import calcula_ecg, calcaula_ecg_p
from tilix_model import calcular_tilix
from flask_sqlalchemy import SQLAlchemy

import matplotlib.pyplot as plt

app = Flask(__name__)

#Conexeão com banco de dados
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/python'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

#Classe para construir a tabela Resultado
class Resultado(db.Model):
    _tablename_ = 'resultado'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    resultado = db.Column(db.String(10))
    legenda = db.Column(db.String(30))
    idade = db.Column(db.Integer)
    sexo = db.Column(db.String(1))
    escala = db.Column(db.String(20))
#Construtor da Classe
    def __init__(self, resultado, legenda, idade, sexo, escala):
        self.resultado = resultado
        self.legenda = legenda
        self.idade = idade
        self.sexo = sexo
        self.escala = escala
db.create_all()

#Rotas - Direcionamentos das páginas  HTML
@app.route('/')
def principal():
    return render_template('index.html')


@app.route('/ecg')
def req_ecg():
    return render_template('ecg.html', titulopagina='Escala de Coma de Glasgow')


@app.route('/avch')
def req_avch():
    return render_template('avch.html', titulopagina='Escala - Acidente Vascular Cerebral Hemorragico AVCH')


@app.route('/tilics')
def req_tilix():
    return render_template('tilics.html', titulopagina='Escala  Tilics')

#Rotas com chamadas para realização de funções com métodos Posts e Gets e redirecionamento de páginas
@app.route('/calcularecg', methods=['POST', ])
def metodo_ecg():
    if not request.values.__len__() != 3:
        aberturaOcular = request.form['ck_ab']
        respostaVerbal = request.form['ck_rv']
        respostaMotora = request.form['ck_rm']

        #Recebimento de resultados(como tuplas), após chamadas das funções
        totalEcg, mensagem = calcula_ecg(aberturaOcular, respostaVerbal, respostaMotora)
        return render_template('ecg.html', titulo='Resultado', resultado=totalEcg, legenda=mensagem, escala='ecg',
                               titulopagina='Escala de Coma de Glasgow')
    else:
        return render_template('ecg.html',
                               retornoErro='É necessário selecionar pelo menos uma das opções do parâmetros abaixo, para realizar o cálculo')


@app.route('/calculaecgp')
def metodo_calcula_pupila():
    pupilaEsqueda = request.args.get('pp_es')
    pupilaDireita = request.args.get('pp_dr')
    resultado = request.args.get('resultado')
    try:
        total, mensagem = calcaula_ecg_p(pupilaEsqueda, pupilaDireita, resultado)
        return render_template('ecg.html', resultado=total, legenda=mensagem, escala='ecg')
    except:
        return render_template('ecg.html',
                               retornoErro='É necessário selecionar pelo menos uma das opções do parâmetros abaixo, para realizar o cálculo')

@app.route('/calcularavch', methods=['POST', ])
def metodo_avch():
    if not request.values.__len__() != 5:
        ecg = request.form.get('ecg')
        idade = request.form.get('idade')
        l_hematoma = request.form.get('lh')
        v_hematoma = request.form.get('vl_hm')
        hemoventriculo = request.form.get('hemo')

        score_avch, mensagem = calcular_avch(ecg, idade, l_hematoma, v_hematoma, hemoventriculo)

        return render_template('avch.html', resultado=score_avch, legenda=mensagem, escala='avch')
    else:
        return render_template('avch.html', retornoErro='É necessário selecionar pelo menos uma das opções dos dados clinicos')

@app.route('/calculartilix', methods=['POST', ])
def metodo_tilix():
    if not request.values.__len__() != 3:
        morfologiaFratura = request.form.get('mf')
        complexoLigamentar = request.form.get('clp')
        estadoNeurologico = request.form.get('en')
        resultado, mensagem = calcular_tilix(morfologiaFratura, complexoLigamentar, estadoNeurologico)

        return render_template('tilics.html', resultado=resultado, legenda=mensagem, escala='tilics')
    else:
        return render_template('tilics.html', retornoErro='É necessário selecionar pelo menos uma das opções')

@app.route('/cadastrar_resultado', methods=['POST', ])
def metodo_gravar():
    resultado = request.form.get('resultado')
    legenda = request.form.get('legenda')
    idade = request.form.get('idade')
    sexo = request.form.get('sexo')
    escala = request.form.get('escala')
    try:
        r = Resultado(resultado, legenda, idade, sexo, escala)
        db.session.add(r)
        db.session.commit()

        return render_template(escala+'.html', retorno="Dados cadastrados com sucesso!!")
    except:
        return render_template('index.html',retorno="Erro ao cadastrar  dados!")


#Funcão para gerar relatórios, execução de queries
@app.route('/listar', methods=['GET', ])
def metodo_listar():

    escala = request.args.get('escala')

    if escala==None:
        listar = Resultado.query.all()
    else:
       listar = Resultado.query.filter_by(escala=escala)

    return render_template('lista.html', relatorio=listar)


#Função para exibição de gráficos utilizando bibliotecas
@app.route('/graficos', methods=['GET', ])
def metodos_graficos():
    avch = Resultado.query.filter_by(escala='avch').count()
    ecg = Resultado.query.filter_by(escala='ecg').count()
    tilix = Resultado.query.filter_by(escala='tilics').count()

    y_axis = [avch, ecg, tilix]
    x_axis = ['AVCH', 'ECG', 'TILICS']

    width_n = 0.4
    bar_color = 'magenta'
    plt.bar(x_axis, y_axis, width=width_n, color=bar_color)
    plt.savefig('static/img/total')

    return render_template('graficos.html')

#Execução Principal
if __name__ == '__main__':
    app.run(debug=True)
