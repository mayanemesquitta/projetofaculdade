#Função para realizar o calculo da escala ECG
def calcula_ecg(aberturaOcular, respostaVerbal, respostaMotora):
    totalEcg = int(aberturaOcular) + int(respostaVerbal) + int(respostaMotora)
    mensagem = calculoLegenda(totalEcg)
    return totalEcg, mensagem

#Função para realizar o calculo da escala de Glasgow P
def calcaula_ecg_p(pupilaEsquerda, pupilaDireita, resultado):
    if resultado=='':resultado=0
    totalP = int(resultado) - (int(pupilaDireita) + int(pupilaEsquerda))
    mensagem = calculoLegenda(totalP)
    return totalP, mensagem

#Função para definir a legenda da escala ECG
def calculoLegenda(valor):
    mensagem = ''
    if valor >= 3 and valor <= 8:
        mensagem = "Trauma grave"
    elif valor >= 9 and valor <= 12:
        mensagem = "Trauma moderado"
    elif valor >= 13 and valor <= 15:
        mensagem = "Trauma leve"
    return mensagem
